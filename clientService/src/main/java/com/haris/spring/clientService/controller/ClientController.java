package com.haris.spring.clientService.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

import com.haris.spring.clientService.model.Account;
import com.haris.spring.clientService.model.AccountRequest;
import com.haris.spring.clientService.service.ClientService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;



/**
 * ControllerClient
 */
@Controller
@Slf4j
public class ClientController {

    private ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping(value="/")
    public String Index(Model model) {
        model.addAttribute("loginRequest", new AccountRequest());

        return "index.html";
    }
    

    @PostMapping(value="/api/v1/client/login")
    public String Login(@ModelAttribute AccountRequest request, Model model) throws Exception {
        log.info(String.format("username : %s\n", request.getUsername()));

        Account account = clientService.validate(request);

        log.info("account : " + account);

        model.addAttribute("account", account);
        return "homepage.html";
    }
    
}