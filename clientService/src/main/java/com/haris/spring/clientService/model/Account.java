package com.haris.spring.clientService.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Account
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Account {

    private String ipAddress;
    private String username;
    private String name;
    private String email;
}