package com.haris.spring.clientService.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * LoginRequest
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountRequest {

    private String username;
    private String password;
    
}