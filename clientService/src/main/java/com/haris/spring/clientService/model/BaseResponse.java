package com.haris.spring.clientService.model;

import io.micrometer.core.lang.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * BaseResponse
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BaseResponse {

    private int statusCode;
    private String description;
    @Nullable
    private Account data;
    
}