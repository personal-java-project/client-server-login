package com.haris.spring.clientService.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Token
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Token {

    @JsonProperty(value = "access_token")
    private String accessToken;

    @JsonProperty(value = "token_type")
    private String tokenType;

    @JsonProperty(value = "refresh_token")
    private String refreshToken;

    @JsonProperty(value = "expire_in")
    private int expireIn;
    private String scope;
    private String jti;
}