package com.haris.spring.clientService.service;

import java.nio.charset.Charset;

import com.haris.spring.clientService.model.*;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

/**
 * ClientService
 */
@Service
@Slf4j
public class ClientService {

    @Value("${server.url:localhost}")
    private String url;

    @Value("${server.api.getAccount}")
    private String getAccountAPI;

    @Value("${server.api.login}")
    private String loginAPI;

    @Value("${server.oauth.clientId}")
    private String clientId;

    @Value("${server.oauth.secretKey}")
    private String secretKey;

    private RestTemplate restTemplate;

    public ClientService(@Nullable RestTemplate restTemplate) {
        if (restTemplate == null) {
            this.restTemplate = new RestTemplate();
        } else {
            this.restTemplate = restTemplate;
        }
    }

    public Account validate(AccountRequest request) throws Exception {
        log.info("url : " + url);
        log.info("request : " + request);

        HttpHeaders headerLogin = new HttpHeaders() {{
            String auth = clientId+":"+secretKey;
            byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")) );
            String authHeader = "Basic " + new String(encodedAuth);

            set("Authorization", authHeader);
        }};
        headerLogin.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type", "password");
        map.add("username", request.getUsername());
        map.add("password", request.getPassword());

        HttpEntity<MultiValueMap<String, String>> entityLogin = new HttpEntity<MultiValueMap<String, String>>(map,headerLogin);

        ResponseEntity<Token> responseToken = restTemplate.exchange(url+loginAPI, HttpMethod.POST, entityLogin, Token.class);

        Token token = responseToken.getBody();

        HttpHeaders headerAuthorized = new HttpHeaders() {{
            set("Authorization", token.getTokenType()+" "+token.getAccessToken());
        }};

        HttpEntity<LoginRequest> entityGetAccount = new HttpEntity<LoginRequest>(LoginRequest.builder().username(request.getUsername()).build(), headerAuthorized);

        ResponseEntity<BaseResponse> response = restTemplate.exchange(url+getAccountAPI, HttpMethod.POST, entityGetAccount , BaseResponse.class);
        log.info("response : " + response);
        return response.getBody().getData();
    }

}