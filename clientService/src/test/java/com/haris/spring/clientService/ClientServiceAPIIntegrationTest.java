package com.haris.spring.clientService;

import com.haris.spring.clientService.controller.ClientController;
import com.haris.spring.clientService.model.AccountRequest;
import com.haris.spring.clientService.service.ClientService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceAPIIntegrationTest {

    @InjectMocks
    private ClientController clientController;

    private ClientService clientService;

    private MockMvc mockMvc;

    @Before
    public void init() {
        clientService = new ClientService(null);
        clientController = new ClientController(clientService);

        mockMvc = standaloneSetup(clientController).build();
    }

    @Test
    public void APIIntegrationTest() throws Exception {
        ReflectionTestUtils.setField(clientService, "url", "http://149.28.157.188:8181");
        ReflectionTestUtils.setField(clientService, "getAccountAPI","/v1/api/getAccount");
        ReflectionTestUtils.setField(clientService, "loginAPI","/oauth/login");
        ReflectionTestUtils.setField(clientService, "clientId","client-server");
        ReflectionTestUtils.setField(clientService, "secretKey","secret");


        AccountRequest request = AccountRequest.builder()
                                .username("admin")
                                .password("admin")
                                .build();

        mockMvc.perform(post("/api/v1/client/login")
        .flashAttr("accountRequest", request))
        .andExpect(forwardedUrl("homepage.html"));
    }

}
