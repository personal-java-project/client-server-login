package com.haris.spring.clientService;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.haris.spring.clientService.controller.ClientController;
import com.haris.spring.clientService.model.Account;
import com.haris.spring.clientService.model.AccountRequest;
import com.haris.spring.clientService.service.ClientService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceAPITests {

    @Mock
    private ClientService clientService;

    @InjectMocks
    private ClientController clientController;

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @Before
    public void init() {
        objectMapper = new ObjectMapper();
        clientController = new ClientController(clientService);
        mockMvc = standaloneSetup(clientController).build();
    }

    @Test
    public void APIUnitTests() throws Exception{

        AccountRequest request = AccountRequest.builder()
                                .username("admin")
                                .password("admin")
                                .build();

        Account account = Account.builder()
                            .ipAddress("localhost")
                            .username("admin")
                            .name("admin")
                            .email("email@email.com")
                            .build();

        when(clientService.validate(any())).thenReturn(account);

        System.out.println("request : " + objectMapper.writeValueAsString(request));

        mockMvc.perform(post("/api/v1/client/login")
            .flashAttr("accountRequest", request))
            .andExpect(forwardedUrl("homepage.html"));
    }
}
