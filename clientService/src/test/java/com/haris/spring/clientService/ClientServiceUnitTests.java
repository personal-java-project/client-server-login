package com.haris.spring.clientService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.haris.spring.clientService.model.Account;
import com.haris.spring.clientService.model.BaseResponse;
import com.haris.spring.clientService.model.AccountRequest;
import com.haris.spring.clientService.model.Token;
import com.haris.spring.clientService.service.ClientService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

@RunWith(MockitoJUnitRunner.class)
@AutoConfigureMockMvc
public class ClientServiceUnitTests {

    @Mock
    private AccountRequest accountRequest;

    @Mock
    private BaseResponse baseResponse;

    @Mock
    private Account account;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private ClientService clientService;

    private String loginAPI = "/oauth/login";
    private String getAccountAPI = "/v1/api/getAccount";
    private String url = "http://localhost:8181";

    @Before
    public void init() {
        accountRequest = AccountRequest.builder()
                        .username("admin")
                        .build();
        account = Account.builder()
                    .ipAddress("127.0.0.1")
                    .username("admin")
                    .name("admin")
                    .build();
        baseResponse = BaseResponse.builder()
                        .data(account)
                        .description("success")
                        .statusCode(200)
                        .build();
        clientService = new ClientService(restTemplate);

        ReflectionTestUtils.setField(clientService, "url", url);
        ReflectionTestUtils.setField(clientService, "getAccountAPI", getAccountAPI);
        ReflectionTestUtils.setField(clientService, "loginAPI", loginAPI);
    }

    @Test
    public void validateTest() throws Exception {
        Account expectedResult = account;

        Token token = Token.builder()
                .accessToken("asd123213")
                .refreshToken("123asdsasd")
                .expireIn(12312)
                .jti("12312")
                .tokenType("bearer")
                .build();

        when(restTemplate.exchange(
                ArgumentMatchers.eq(url+getAccountAPI)
                , any()
                , any()
                , ArgumentMatchers.<Class<BaseResponse>>any())).thenReturn(new ResponseEntity<>(baseResponse, HttpStatus.OK));

        when(restTemplate.exchange(
                ArgumentMatchers.eq(url+loginAPI)
                , any()
                , any()
                , ArgumentMatchers.<Class<Token>>any())).thenReturn(new ResponseEntity<>(token, HttpStatus.OK));

        Account result = clientService.validate(accountRequest);
        System.out.println("result : " + result);

        assertEquals(result.getName(), expectedResult.getName());

    }
}
