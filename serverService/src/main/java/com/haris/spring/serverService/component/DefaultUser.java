package com.haris.spring.serverService.component;

import com.haris.spring.serverService.model.Account;
import com.haris.spring.serverService.model.Authority;
import com.haris.spring.serverService.model.Role;
import com.haris.spring.serverService.model.User;
import com.haris.spring.serverService.repository.AccountRepository;
import com.haris.spring.serverService.repository.AuthorityRepository;
import com.haris.spring.serverService.repository.RoleRepository;
import com.haris.spring.serverService.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Set;

@Component
public class DefaultUser {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private AccountRepository accountRepository;

    @PostConstruct
    public void createAdmin() {
        Authority authorityWrite = Authority.builder()
                .code("write")
                .name("write")
//                .idAuthority(1L)
                .build();
        Authority authorityRead = Authority.builder()
                .code("read")
                .name("read")
//                .idAuthority(2L)
                .build();

        authorityRepository.save(authorityRead);
        authorityRepository.save(authorityWrite);

        Role role = Role.builder()
                .name("admin")
                .description("admin")
                .authorities(Set.of(authorityWrite, authorityRead))
//                .idRole(1L)
                .build();

        roleRepository.save(role);

        Account account = Account.builder()
//                .idAccount(1L)
                .name("administrator")
                .address("office")
                .email("help@admin.com")
                .phone("+1027464523")
                .build();

        accountRepository.save(account);

        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

        User user = User.builder()
//                .id(1L)
                .name("admin")
                .username("admin")
                .password(bCryptPasswordEncoder.encode("admin"))
                .account(account)
                .roles(Set.of(role))
                .build();

        userRepository.save(user);
    }
}
