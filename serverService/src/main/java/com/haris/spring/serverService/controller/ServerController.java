package com.haris.spring.serverService.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;

import com.haris.spring.serverService.model.BaseResponse;
import com.haris.spring.serverService.model.AccountRequest;
import com.haris.spring.serverService.service.ServerAccountService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;



@RestController
@RequestMapping(value = "v1/api")
@Slf4j
public class ServerController {

    private ServerAccountService serverAccountService;

    public ServerController(ServerAccountService serverAccountService) {
        this.serverAccountService = serverAccountService;
    }

    @PostMapping(value="/getAccount")
    public ResponseEntity<BaseResponse> getAccount(@RequestBody AccountRequest entity, HttpServletRequest request) throws Exception {

        log.info("request : " + entity);

        Object result = serverAccountService.login(entity, request);

        ResponseEntity response;
        if (result != null) {
            response = new ResponseEntity<>(new BaseResponse(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), result), HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(new BaseResponse(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.getReasonPhrase(), result), HttpStatus.NOT_FOUND);
        }
        log.info("response : " + response);
        return response;
    }
}