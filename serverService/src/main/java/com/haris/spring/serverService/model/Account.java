package com.haris.spring.serverService.model;

import javax.annotation.Generated;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * Account
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table
public class Account {

    @Column(name = "id_account")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idAccount;
    private String name;
    private String email;
    private String phone;
    private String address;
    
}