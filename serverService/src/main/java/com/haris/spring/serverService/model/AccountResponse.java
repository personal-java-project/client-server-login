package com.haris.spring.serverService.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * AccountResponse
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountResponse {

    private String ipAddress;
    private String username;
    private String name;
    private String email;
    
}