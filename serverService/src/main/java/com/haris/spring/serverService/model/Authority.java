package com.haris.spring.serverService.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table
public class Authority implements GrantedAuthority {

    @Column(name = "id_authority")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAuthority;
    private String name;
    private String code;

    @Override
    public String getAuthority() {
        return code;
    }
}
