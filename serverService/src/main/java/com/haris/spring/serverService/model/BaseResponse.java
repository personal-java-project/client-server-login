package com.haris.spring.serverService.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * BaseResponse
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseResponse {

    private int statusCode;
    private String description;
    private Object data;
    
}