package com.haris.spring.serverService.model;

import jdk.jfr.Enabled;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table
public class Role {

    @Column(name = "id_role")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idRole;
    private String name;
    private String description;

    @ManyToMany
    @JoinTable(name = "role_authority"
                , joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id_role")
                , inverseJoinColumns = @JoinColumn(name = "authority_id", referencedColumnName = "id_authority"))
    private Set<Authority> authorities;

}