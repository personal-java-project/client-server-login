package com.haris.spring.serverService.model;

//import com.haris.spring.serverService.component.StaticApplicationContext;
//import com.haris.spring.serverService.config.ApplicationProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table
public class User implements UserDetails {

    @Column(name = "id_user")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUser;
    private String username;
    private String password;
    private Integer failedLoginAttemptCount;
    private LocalDateTime lastFailedLoginDate;
    private UserStatus userStatus;
    private String name;

    @ManyToMany
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id_user"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id_role"))
    private Set<Role> roles;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_account"
            , referencedColumnName = "id_account"
            , foreignKey = @ForeignKey(name = "fk_id_account")
            , nullable = false)
    private Account account;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<Authority> authorities = new HashSet<>();
        if (roles != null) {
            roles.forEach(role -> {
                if (role.getAuthorities() != null) {
                    authorities.addAll(role.getAuthorities());
                }
            });
        }
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
//        if (userStatus == UserStatus.ACTIVE) {
//            return true;
//        } else if (userStatus == UserStatus.TEMP_LOCKED_BAD_CREDENTIALS && lastFailedLoginDate != null) {
//            return lastFailedLoginDate.plusSeconds(StaticApplicationContext.getApplicationContext()
//                    .getBean(ApplicationProperties.class).getAuth().getFailedLoginAttemptAccountLockTimeout())
//                    .isBefore(LocalDateTime.now());
//        } else {
//            return false;
//        }

        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
