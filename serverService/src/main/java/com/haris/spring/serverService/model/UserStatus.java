package com.haris.spring.serverService.model;

import lombok.Getter;

public enum UserStatus {

    ACTIVE("Active", "A"),
    INACTIVE("Inactive", "I"),
    DELETED("Deleted", "D"),
    CREATED("Created", "C"),
    PENDING_ACTIVATION("Pending Activation", "PEA"),
    TEMP_LOCKED_BAD_CREDENTIALS("Temp Locked Bad Credentials", "TELBC");

    @Getter
    private String label;

    @Getter
    private String value;

    private UserStatus(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public static UserStatus getEnum(String value) {
        for (UserStatus userStatus : UserStatus.values()) {
            if (userStatus.getValue().equalsIgnoreCase(value)) {
                return userStatus;
            }
        }
        return null;
    }
}
