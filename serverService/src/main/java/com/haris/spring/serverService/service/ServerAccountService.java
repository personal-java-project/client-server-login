package com.haris.spring.serverService.service;

import javax.servlet.http.HttpServletRequest;

import com.haris.spring.serverService.model.*;

import com.haris.spring.serverService.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;

import lombok.extern.slf4j.Slf4j;

/**
 * ServerLoginService
 */
@Service
@Slf4j
public class ServerAccountService {

    @Autowired
    private UserRepository userRepository;
    
	private final String LOCALHOST_IPV4 = "127.0.0.1";
	private final String LOCALHOST_IPV6 = "0:0:0:0:0:0:0:1";

    public Object login(AccountRequest loginRequest, HttpServletRequest request) throws Exception {

        String ipAddress = getIpAddress(request);

        User user = userRepository.findByUsername(loginRequest.getUsername());

        if (user == null) {
            return null;
        }
        return new AccountResponse(ipAddress, user.getUsername(), user.getAccount().getName(), user.getAccount().getEmail());
    }

    public String getIpAddress(HttpServletRequest request) {

        if (request != null) {

            String ipAddress = request.getHeader("X-Forwarded-For");
            if(StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("Proxy-Client-IP");
            }

            if(StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("WL-Proxy-Client-IP");
            }

            if(StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getRemoteAddr();
                if(LOCALHOST_IPV4.equals(ipAddress) || LOCALHOST_IPV6.equals(ipAddress)) {
                    try {
                        InetAddress inetAddress = InetAddress.getLocalHost();
                        ipAddress = inetAddress.getHostAddress();
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }
                }
            }

            if(!StringUtils.isEmpty(ipAddress) 
                    && ipAddress.length() > 15
                    && ipAddress.indexOf(",") > 0) {
                ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
            }

            log.info("ip address : " + ipAddress);
            return ipAddress;
        }
        return null;
    }
}
