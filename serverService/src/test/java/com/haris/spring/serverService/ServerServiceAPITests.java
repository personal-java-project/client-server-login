package com.haris.spring.serverService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.haris.spring.serverService.controller.ServerController;
import com.haris.spring.serverService.model.AccountResponse;
import com.haris.spring.serverService.model.BaseResponse;
import com.haris.spring.serverService.model.AccountRequest;
import com.haris.spring.serverService.service.ServerAccountService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import java.net.InetAddress;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RunWith(MockitoJUnitRunner.class)
@AutoConfigureMockMvc
public class ServerServiceAPITests {

    @Mock
    private ServerAccountService serverLoginService;

    @InjectMocks
    private ServerController serverController;

    private MockMvc mockMvc;

    @Before
    public void init() {
        serverController = new ServerController(serverLoginService);
        mockMvc = standaloneSetup(serverController).build();
    }

    @Test
    public void loginTests() throws Exception {

        // HttpServletRequest request = new MockHttpServletRequest();

        String localIPAddress = InetAddress.getLocalHost().getHostAddress();

        ObjectMapper objectMapper = new ObjectMapper();

        AccountRequest loginRequest = AccountRequest.builder()
                                        .username("admin")
                                        .build();

        AccountResponse accountResponse = AccountResponse.builder()
                                    .ipAddress(localIPAddress)
                                    .name("admin")
                                    .username("admin")
                                    .build();

        when(serverLoginService.login(any(), any())).thenReturn(accountResponse);

        HttpServletResponse result = mockMvc.perform(post("/v1/api/getAccount")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(loginRequest)))
            .andReturn().getResponse();

        assertEquals(HttpStatus.OK.value(), result.getStatus());
    }

    @Test
    public void loginTestsMethod() throws Exception {
        
        HttpServletRequest request = new MockHttpServletRequest();

        String localIPAddress = InetAddress.getLocalHost().getHostAddress();

        AccountRequest loginRequest = AccountRequest.builder()
                                        .username("admin")
                                        .build();

        AccountResponse accountResponse = AccountResponse.builder()
                                    .ipAddress(localIPAddress)
                                    .name("admin")
                                    .username("admin")
                                    .build();

        when(serverLoginService.login(any(), any())).thenReturn(accountResponse);

        ResponseEntity<BaseResponse> result = serverController.getAccount(loginRequest, request);
        assertEquals(accountResponse.getIpAddress(), ((AccountResponse)result.getBody().getData()).getIpAddress());
    }
}
