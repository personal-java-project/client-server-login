package com.haris.spring.serverService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.haris.spring.serverService.component.DefaultUser;
import com.haris.spring.serverService.model.*;
import com.haris.spring.serverService.repository.AccountRepository;
import com.haris.spring.serverService.repository.AuthorityRepository;
import com.haris.spring.serverService.repository.RoleRepository;
import com.haris.spring.serverService.repository.UserRepository;
import com.haris.spring.serverService.service.ServerAccountService;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.mock.web.MockHttpServletRequest;

import java.net.InetAddress;

import javax.servlet.http.HttpServletRequest;

@RunWith(MockitoJUnitRunner.class)
@AutoConfigureMockMvc
public class ServerServiceUnitTests {

    @Mock
    private UserRepository userRepository;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private AuthorityRepository authorityRepository;

    @Mock
    private RoleRepository roleRepository;

    @InjectMocks
    private ServerAccountService serverLoginService;

    @InjectMocks
    private DefaultUser defaultUser;

    private String localIPAddress;

    // Before
    @Before
    public void init() throws Exception {
        localIPAddress = InetAddress.getLocalHost().getHostAddress();
    }

    @AfterEach
    public void destroy() {
        verifyNoMoreInteractions(userRepository);
    }

    // Unit Test
    @Test
    public void getIPAddress() throws Exception {
        HttpServletRequest mockRequest = new MockHttpServletRequest();

        String resultIPAddress = serverLoginService.getIpAddress(mockRequest);

        assertEquals(localIPAddress, resultIPAddress);
    }

    @Test
    public void loginTest() throws Exception {
        User user = User.builder()
                .idUser(1L)
                .name("admin")
                .password("admin")
                .username("admin")
                .account(Account.builder()
                        .phone("+0213213")
                        .email("info@admin.com")
                        .name("administrator")
                        .build())
                .build();

        HttpServletRequest request = new MockHttpServletRequest();

        AccountRequest loginRequest = AccountRequest.builder()
                                        .username("admin")
                                        .build();

        AccountResponse expected = AccountResponse.builder()
                                    .ipAddress(localIPAddress)
                                    .name("admin")
                                    .username("admin")
                                    .build();

        when(userRepository.findByUsername(anyString())).thenReturn(user);

        AccountResponse resultAccount = (AccountResponse) serverLoginService.login(loginRequest, request);

        assertEquals(expected.getIpAddress(), resultAccount.getIpAddress());
    }

    @Test
    public void createDefaultAccount() {
        Role role = Role.builder()
                .name("admin")
                .build();
        Authority authority = Authority.builder()
                .name("read")
                .code("read")
                .build();
        Account account = Account.builder()
                .phone("+0213213")
                .email("info@admin.com")
                .name("administrator")
                .build();
        User user = User.builder()
                .idUser(1L)
                .name("admin")
                .password("admin")
                .username("admin")
                .build();
        when(authorityRepository.save(any(Authority.class))).thenReturn(authority);
        when(roleRepository.save(any(Role.class))).thenReturn(role);
        when(accountRepository.save(any(Account.class))).thenReturn(account);


        when(userRepository.save(any(User.class))).thenReturn(user);

        defaultUser.createAdmin();

        verify(userRepository).save(any(User.class));
    }
}
